﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Fireworks
{
    public class EntityController
    {
        static Random rand = new Random();
        List<PrimitiveObject> addList;
        List<PrimitiveObject> objList;
        bool isUpdating;

        public EntityController()
        {
            objList = new List<PrimitiveObject>();
            addList = new List<PrimitiveObject>();
            Random seed = new Random();

            CreateMoonAndStars();
        }

        private void CreateMoonAndStars()
        {
            Vector2 moonPosition = new Vector2(300, 100);
            int starCount = 200;

            PrimitiveObject moon;
            moon = new PrimitiveObject(moonPosition, Art.moon, Color.LightYellow);
            Add(moon);

            Random rand = new Random();

            for (int i = 0; i < starCount; i++)
            {
                PrimitiveObject star;
                Vector2 pos = new Vector2(rand.Next(0 ,(int)Game1.windowSize.X), 
                   rand.Next((int)Game1.windowSize.Y));

                star = new PrimitiveObject(pos, Art.star, Color.LightYellow);
                star.scale = new Vector2((float)(rand.NextDouble() + .1f) / 5);
                star.opacity = .7f;
                Add(star);
            }
        }

        public void Update(int millisecs)
        {
            if (rand.Next(50) == 0)
                addFirework();

            isUpdating = true;
            foreach (var obj in objList)
                obj.Update(millisecs);
            isUpdating = false;

            foreach (var obj in addList)
                Add(obj);
            addList.Clear();

            objList.RemoveAll(PrimitiveObject.isDelete);
        }

        public void Draw(SpriteBatch batch)
        {
            foreach (var obj in objList)
                obj.Draw(batch);
        }

        public void Add(PrimitiveObject obj)
        {
            if (isUpdating)
                addList.Add(obj);
            else
                objList.Add(obj);
        }

        private void addFirework()
        {
            //if (rand.Next(0, 1) == 0)
                Add(PrimitiveFirework.getNew());
            //else
            //    Add(RotatingFirework.getNew());
        }
    }
}
